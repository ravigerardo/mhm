class AddTweetIdToPublications < ActiveRecord::Migration[5.2]
  def change
    add_column :publications, :tweet_id, :string
  end
end
