class AddPinIdToPublications < ActiveRecord::Migration[5.2]
  def change
    add_column :publications, :pin_id, :string
  end
end
