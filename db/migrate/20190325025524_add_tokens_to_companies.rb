class AddTokensToCompanies < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :token, :string
    add_column :companies, :secret, :string
    add_column :companies, :uid, :string
    add_column :companies, :image, :string
  end
end
