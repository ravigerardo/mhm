class CreateCampaigns < ActiveRecord::Migration[5.2]
  def change
    create_table :campaigns do |t|
      t.string :title
      t.string :hashtag
      t.string :color

      t.timestamps
    end
  end
end
