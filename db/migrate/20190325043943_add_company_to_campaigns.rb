class AddCompanyToCampaigns < ActiveRecord::Migration[5.2]
  def change
    add_reference :campaigns, :company, foreign_key: true
  end
end
