class WorkgroupsController < ApplicationController
  def index
    @workgroup = User.where(company: current_user.company)
  end

  def get_user
    user = User.find(params[:id])
    if user.company_id == current_user.company_id && current_user.is_admin
      render json: user
    else
      redirect_to '/workgroup', alert: "You do not have sufficient permissions for this action"
    end
  rescue ActiveRecord::RecordNotFound
    redirect_to '/workgroup', alert: "Nonexistent user"
  end

  def create
    if current_user.is_admin
      @user = User.new(user_params)
      @user.company = current_user.company
      
      if @user.save
        redirect_to '/workgroup', notice: "Now #{params[:user][:username]} is part of your workgroup."
      else
        redirect_to '/workgroup', alert: @user.errors
      end
    else
      redirect_to '/workgroup', alert: 'You are not an administrator'
    end
  end

  def destroy
    if current_user.is_admin
      User.find(params[:id]).destroy
      redirect_to '/workgroup', notice: "User deleted."
    else
      redirect_to '/workgroup', alert: 'You are not an administrator'
    end
  end

  private
    def user_params
      params.require(:user).permit(:username, :email, :password, :password_confirmation)
    end
end
