class CompaniesController < ApplicationController
    def new
    end

    def create
        @company = Company.new(company_params)
        @user = @company.users.new(user_params)
        @user.update(is_admin: true)
        if @company.save 
            sign_in @user
            redirect_to '/auth/twitter/'
        else
            redirect_to '/', notice: @company.errors
        end
    end

    private

        def company_params
            params.require(:company).permit(:name, :package)
        end

        def user_params
            params.require(:user).permit(:username, :email, :password, :password_confirmation)
        end
end
