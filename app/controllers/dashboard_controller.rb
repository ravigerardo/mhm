class DashboardController < ApplicationController
  before_action :authenticate_user!
  def index
    @campaigns = Campaign.where(company_id: current_user.company_id)
    @campaign = Campaign.new
  end
end
