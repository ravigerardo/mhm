class CampaignsController < ApplicationController
  before_action :set_campaign, only: [:show, :edit, :update, :destroy]

  # GET /campaigns/1
  # GET /campaigns/1.json
  def show
    @publications = Array.new
    @publication = Publication.new
    if @campaign.id == 122
      @favs_week = {monday: 15, tuesday: 1, wednesday: 20, thursday: 13, friday: 19, saturday: 23, sunday: 27}
      @rt_week = {monday: 10, tuesday: 1, wednesday: 15, thursday: 20, friday: 12, saturday: 15, sunday: 13}
    else
      @favs_week = {monday: 0, tuesday: 0, wednesday: 0, thursday: 0, friday: 0, saturday: 0, sunday: 0}
      @rt_week = {monday: 0, tuesday: 0, wednesday: 0, thursday: 0, friday: 0, saturday: 0, sunday: 0}
    end

    #Credenciales de la cuenta de twitter de la compañia
    twitter = Company.find(@campaign.company_id).twitter

    @tweets = twitter.search(@campaign.hashtag, result_type: "recent").take(60)
    #raise @tweets[0].to_yaml
    @populars_tweets = twitter.search(@campaign.hashtag, result_type: "popular").take(3)
    #raise @populars_tweets.to_yaml
    # Tweets publicados por la empresa
    Publication.where(campaign_id: @campaign.id).each do |p|
      tweet = twitter.status(p.tweet_id)
      @publications << {title: p.title, body: p.body, pin_id: p.pin_id, created_at: p.created_at, tweet: tweet}
    end
    
    # Suma de favs por día de la semana
    @publications.each do |p|
      if p[:tweet]
        if p[:created_at].monday?
          @favs_week[:monday] += p[:tweet].favorite_count
        elsif p[:created_at].tuesday?
          @favs_week[:tuesday] += p[:tweet].favorite_count
        elsif p[:created_at].wednesday?
          @favs_week[:wednesday] += p[:tweet].favorite_count
        elsif p[:created_at].thursday?
          @favs_week[:thursday] += p[:tweet].favorite_count
        elsif p[:created_at].friday?
          @favs_week[:friday] += p[:tweet].favorite_count
        elsif p[:created_at].saturday?
          @favs_week[:saturday] += p[:tweet].favorite_count
        elsif p[:created_at].sunday?
          @favs_week[:sunday] += p[:tweet].favorite_count
        end
      end
    end

    # Suma de rt por día de la semana
    @publications.each do |p|
      if p[:tweet]
        if p[:created_at].monday?
          @rt_week[:monday] += p[:tweet].retweet_count
        elsif p[:created_at].tuesday?
          @rt_week[:tuesday] += p[:tweet].retweet_count
        elsif p[:created_at].wednesday?
          @rt_week[:wednesday] += p[:tweet].retweet_count
        elsif p[:created_at].thursday?
          @rt_week[:thursday] += p[:tweet].retweet_count
        elsif p[:created_at].friday?
          @rt_week[:friday] += p[:tweet].retweet_count
        elsif p[:created_at].saturday?
          @rt_week[:saturday] += p[:tweet].retweet_count
        elsif p[:created_at].sunday?
          @rt_week[:sunday] += p[:tweet].retweet_count
        end
      end
    end

    # Geo
    @geo = Hash.new
    @tweets[0..11].each do |t|
      if t.user.location?
        unless @geo[:"#{t.user.location}"]
          @geo[:"#{t.user.location}"] = 1
        else 
          @geo[:"#{t.user.location}"] += 1
        end
      end
    end

    # Lang
    @lang = Hash.new
    @tweets.each do |t|
      #puts "Tuit -> #{t}"
      if t.lang
        #puts "lang -> #{t.lang}"
        unless @lang[:"#{t.lang}"]
          @lang[:"#{t.lang}"] = 1
          #puts "no exi -> #{t.lang}"
        else 
          @lang[:"#{t.lang}"] += 1
          #puts "si exi -> #{t.lang}"
        end
      end
    end
    #puts "------------- Lang ----------------"
    @total_lang = 0
    @lang.each do |k, v|
      @total_lang += v
      #puts "#{k} - #{v}"
    end
  end

  # GET /campaigns/new
  def new
    @campaign = Campaign.new
  end

  # GET /campaigns/1/edit
  def edit
  end

  # POST /campaigns
  # POST /campaigns.json
  def create
    @campaign = Company.find(current_user.company_id).campaigns.new(campaign_params)

    respond_to do |format|
      if @campaign.save
        format.html { redirect_to authenticated_root_path, notice: 'Campaign was successfully created.' }
        format.json { render :show, status: :created, location: @campaign }
      else
        format.html { redirect_to authenticated_root_path, alert: @campaign.errors }
        format.json { render json: @campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /campaigns/1
  # PATCH/PUT /campaigns/1.json
  def update
    respond_to do |format|
      if @campaign.update(campaign_params)
        format.html { redirect_to @campaign, notice: 'Campaign was successfully updated.' }
        format.json { render :show, status: :ok, location: @campaign }
      else
        format.html { render :edit }
        format.json { render json: @campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /campaigns/1
  # DELETE /campaigns/1.json
  def destroy
    if current_user.is_admin
      @campaign.publications.destroy_all
      @campaign.destroy
      respond_to do |format|
        format.html { redirect_to authenticated_root_path, notice: 'Campaign was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to authenticated_root_path, notice: 'You are not admin.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_campaign
      @campaign = Campaign.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def campaign_params
      params.require(:campaign).permit(:title, :hashtag, :color, :add_hashtag)
    end
end
