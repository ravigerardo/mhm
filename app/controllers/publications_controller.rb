class PublicationsController < ApplicationController
  before_action :set_publication, only: [:destroy]

  # POST /publications
  # POST /publications.json
  def create
    campaign_id = params[:publication][:campaign_id]
    @publication = Campaign.find(campaign_id).publications.new(publication_params)

    # Añadir hashtag a la publicación
    @publication.body += " #{Campaign.find(campaign_id).hashtag}" if params[:add_hashtag]

    respond_to do |format|
      if @publication.save
        format.html { redirect_to "/campaigns/#{campaign_id}", notice: 'Publication was successfully created.' }
        format.json { render :show, status: :created, location: @publication }
      else
        format.html { render :new }
        format.json { render json: @publication.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /publications/1
  # DELETE /publications/1.json
  def destroy
    @publication.destroy
    respond_to do |format|
      format.html { redirect_to publications_url, notice: 'Publication was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def fuckoff
    Publication.delete_all
    Campaign.delete_all
    User.delete_all
    Company.delete_all
    redirect_to unauthenticated_root_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_publication
      @publication = Publication.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def publication_params
      params.require(:publication).permit(:title, :body, :campaign_id, :media)
    end
end
