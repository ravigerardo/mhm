class SessionsController < Devise::SessionsController
    def new
        super do
            
        end
    end

    def assign
        @company = Company.find(current_user.company_id)
        @company.update(token: auth_hash.credentials.token, secret: auth_hash.credentials.secret)
        redirect_to authenticated_root_path
    end

    private 
        def auth_hash
            request.env["omniauth.auth"]
        end
end
