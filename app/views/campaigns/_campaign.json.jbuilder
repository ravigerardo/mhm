json.extract! campaign, :id, :title, :hashtag, :color, :created_at, :updated_at
json.url campaign_url(campaign, format: :json)
