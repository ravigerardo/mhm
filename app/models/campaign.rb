class Campaign < ApplicationRecord
    validates :title, presence: true
    validates :hashtag, presence: true, format: { with: /\A#([a-zA-Z]|[0-9]){2,30}\z/, message: "must start with the symbol of number (#) and to be really productive its length has to be between 2 and 30 characters (lowercase, uppercase and numbers)"}
    validates :color, presence: true
    has_many :publications
    belongs_to :company
end
