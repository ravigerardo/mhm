class Publication < ApplicationRecord
  belongs_to :campaign
  before_create :post_to_twitter
  before_create :post_to_pinterest
  attr_accessor :media
  attr_accessor :add_hashtag

  def post_to_twitter
    if media
      path = media.tempfile.path
      tweet = Company.find(campaign.company_id).twitter.update_with_media(body, path)
    else
      tweet = Company.find(campaign.company_id).twitter.update(body)
    end
    self.tweet_id = tweet.id
  end

  def post_to_pinterest
    if media
      path = media.tempfile.path
      pin = Company.find(campaign.company_id).pinterest.create_pin({
        board: '299841356380747982',
        note: body,
        title: title,
        image: Faraday::UploadIO.new(path, 'image/*')
      })
      self.pin_id = pin.data.id
    end
  end
end
