Rails.application.routes.draw do
  get 'workgroup', to: 'workgroups#index'
  post 'workgroup', to: 'workgroups#create'
  delete 'workgroup/:id', to: 'workgroups#destroy'
  get 'workgroup/user/:id', to: 'workgroups#get_user'
  
  resources :publications, only: [:create, :destroy]
  resources :companies, only: [:new, :create]
  resources :campaigns, except: [:index]
  
  get 'dashboard', to: 'dashboard#index'
  get 'fuckoff', to: 'publications#fuckoff'

  devise_for :users

  devise_scope :user do
    get 'auth/:provider/callback', to: 'sessions#assign'
    
    authenticated :user do
      root 'dashboard#index', as: :authenticated_root
    end
  
    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end
end
